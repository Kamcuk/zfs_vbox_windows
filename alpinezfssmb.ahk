; Only allow one instance of the script to run.
#SingleInstance, Force

; If you don't want an icon to show in the tray,
; remove the semicolon from the line below.
;#NoTrayIcon

;;;; functions ;;;;;;;;

logbuf =

debug(msg) {
	nextline := A_YYYY "." A_MM "." A_DD "T" A_Hour ":" A_Min ":" A_Sec ": " msg
	if ErrorLevel {
		nextline := nextline " " ErrorLevel
	}
	FileAppend, %nextline% `n, C:\alpinezfssmb_ahk.log.txt

	return nextline
}


log(msg) {
	nextline := debug(msg)

	global logbuf
	logbuf := logbuf "`n" nextline
	msg := SubStr(logbuf, -250)
	TrayTip, alpinezfssmb.ahk, %msg%, 10
}

fatal(msg) {
	global logbuf
	log(msg)
	msg := SubStr(logbuf, -2000)
	MsgBox, 1, alpinezfssmb.ahk, %msg%
	Sleep, 5000
	ExitApp
}

is_running() {
	RunWait, %comspec% /c D:\programy\VirtualBox\VBoxManage.exe showvminfo alpinezfssmb | findstr "State:" | findstr "running", , Hide UseErrorLevel
	running := ! ErrorLevel
	if running {
		log("Is running: " running)
	} else {
		log("Is not running: " running)
	}
	return running
}

wait_for_mount() {
	start := A_TickCount
	timeout_s := 120
	stop := start + timeout_s * 1000
	stage := 0
	laststage := 0

	log("Removing X: disc")
	RunAs, Kamil
	RunWait, %comspec% /c net use X: /delete /yes, , Hide

	log("Removing Y: disc")
	RunAs, Kamil
	RunWait, %comspec% /c net use Y: /delete /yes, , Hide

	while (A_TickCount < stop) {
		log("Loop=" stage " to=" (stop - A_TickCount))
	
		; sleep 1 seconds if stalled
		if (laststage == stage) {
			Sleep, 1000
		} else {
			laststage := stage
		
		}
		
		; fail if the machine stopped running
		if (! is_running()) {
			fatal("Machine stoppped running")
		}
		
		; state machine
		if (stage == 0) {
			log("Ping")
			stage++
		} else if (stage == 1) {
			RunAs, Kamil
			RunWait, %comspec% /c ping -w 1000 -s 1 -n 1 192.168.56.2 | findstr TTL=, , Hide UseErrorLevel
			if ErrorLevel {
				continue
			}
			log("Registering network drive")
			stage++
		} else if (stage == 2) {
			RunAs, Kamil
			RunWait, %comspec% /c net use Y: \\192.168.56.2\zfs < NUL, , Hide UseErrorLevel
			if ErrorLevel {
				continue
			}
			log("Drive Y registered")
			stage++
		} else if (stage == 3) {	
			RunAs, Kamil
			RunWait, %comspec% /c net use X: \\192.168.56.2\windows < NUL, , Hide UseErrorLevel
			if ErrorLevel {
				continue
			}
			log("Drive X registered")
			stage++
		} else if (stage == 4) {
			RunAs, Kamil
			RunWait, %comspec% /c ie4uinit.exe -ClearIconCache, , Hide
			log("Refreshing icon cache")
			stage++
		} else if (stage == 5) {
			break
		} else {
			fatal("internal error in state machine" . stage)
		}
	}
	
	; handle statemachine timeout errors
	if (stage == 0) {
		fatal("very bad error")
	} else if (stage == 1) {
		fatal("failed to ping 192.168.56.2")
	} else if (stage == 2) {
		fatal("net use Y: \\192.168.56.2\zfs errored")
	} else if (stage == 3) {
		fatal("net use X: \\192.168.56.2\windows errored")
	}
	
	log("All network drivers connected and machine is up")
}

start() {
	if false {
		log("Setting Shutdown as DefaultCloseAction")
		RunWait, %comspec% /c D:\programy\VirtualBox\VBoxManage.exe setextradata alpinezfssmb GUI/DefaultCloseAction Shutdown, , Hide UseErrorLevel
		if (ErrorLevel) {
			fatal("Setting shutdown as DefaultCloseAction failed")
		}
	}

	if (! is_running()) {
		log("Starting machine")
		RunAs
		RunWait, D:\programy\VirtualBox\VBoxManage.exe startvm alpinezfssmb --type gui, , Hide UseErrorLevel
		if (ErrorLevel) {
			fatal("starting machine failed")
		}
		hide_window()
	} else {
		log("Machine already running...")
	}
}

hide_window() {
	log("Waiting for window for max 40 seconds")
	WinWait, alpinezfssmb [Uruchomiona] - Oracle VM VirtualBox, , 40
	if ErrorLevel {
		fatal("window didnt show up")
	}
	Sleep, 1000
	log("Hiding window")
	WinActivate
	WinHide
	WinHide
	WinHide
	WinHide
}

stop_once = 0
stop() {
	global stop_once
	log("stop_once = " stop_once)
	if (stop_once) {
		return
	}
	stop_once = 1
	
	; Run the batch file.
	log("Stoping alpinezfssmb virtual machine")
	RunAs
	RunWait, D:\programy\VirtualBox\VBoxManage.exe controlvm alpinezfssmb acpipowerbutton, , Hide
	log("Deleting X: disc")
	RunAs, Kamil
	RunWait, %comspec% /c net use X: /delete /yes, , Hide
	log("Deleting Y: disc")
	RunAs, Kamil
	RunWait, %comspec% /c net use Y: /delete /yes, , Hide
	
	log("Waiting for vm close")
	while (is_running()) {
		Sleep, 100
	}
	log("Vm closed")
}

register_shutdown() {
	OnMessage(WM_QUERYENDSESSION, "on_shutdown")
	OnMessage(WM_QUIT, "on_shutdown")
	OnExit("on_shutdown")
}

on_shutdown() {
	stop()
	ExitApp
}

;;;;; main ;;;;;;;;;;;;;;;;

; When script detects WM_QUERYENDSESSION (a shutdown), run OnShutDown function.

start()
register_shutdown()
wait_for_mount()
Pause
